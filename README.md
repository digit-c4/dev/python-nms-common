# Common NMS Python libraries

Consider reading [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices) first

## Usage

```shell
poetry add git+https://code.europa.eu/digit-c4/dev/python-nms-common.git
# or using pip
# pip install git+https://code.europa.eu/digit-c4/dev/python-nms-common.git
python
> import nms_common
> nms_common.html.html_rendering("a\nb")
```

## Troubleshooting

If the ```poetry add``` command fails with the error message ```name 'urllib3' is not defined```. Set the
**POETRY_EXPERIMENTAL_SYSTEM_GIT_CLIENT** environment variable to true
using ```export POETRY_EXPERIMENTAL_SYSTEM_GIT_CLIENT=true```.

## Logging

This package provides everything you need to match the Logging best practices in few lines, including for legacy
existing projects

```python
import logging
from nms_common import nms_logging

logger = nms_logging.getAndConfigureLogger(name="my_logger", level=logging.DEBUG, owner_name="ntx", app_name="my-app")
logger.info("This app is using standardised logging")
```

or split handler configuration and logger loading

```python
import logging
from nms_common import nms_logging

# Configure the handler format
nms_logging.configure_handler(owner_name="ntx", app_name="my-app")
# Then load a logger (that is using the previously configured handler)
logger = nms_logging.getLogger(name="my_logger", level=logging.DEBUG)
logger.info("This app is using standardised logging")
```

Each logger is configured using

| parameter name | environment variable | default value                 |
|----------------|----------------------|-------------------------------|
| `name`         | `LOG_LOGGER`         | `"root"`                      |
| `level`        | `LOG_LEVEL`          | `logging.INFO`                |
| `app_name`     | `NMS_APP_NAME`       | ⚠️ no default value, required |
| `app_type`     | `NMS_APP_TYPE`       | `"app"`                       |
| `owner_name`   | `NMS_OWNER_NAME`     | ⚠️ no default value, required |
| `owner_type`   | `NMS_OWNER_TYPE`     | `"team"`                      |

For existing projects, using the `logging.error()` or `logging.xxx()` shortcuts, the default root logger can be
configured to use the standard formatting in one line

```python
# This line only need to be imported once. Ideally at the very top of 
# root (__main__) file of your script
from nms_common import nms_logging_auto

import logging

logging.error("this error message matches guidelines")
```

NB: to maximise backward compatibility, `app_name` and `owner_name` won't trigger an error if not provided in env when
using `nms_logging_auto`. Only a warning will print the console.
