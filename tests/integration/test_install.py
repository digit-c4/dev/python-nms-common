import logging
import os
import unittest
import subprocess


class TestInstall(unittest.TestCase):

    def test_install_locally(self):
        root = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..'))
        process = subprocess.Popen([
            "docker", "run", "-v", f"{root}:/library",
            "code.europa.eu:4567/digit-c4/dev/python-best-practices/python-poetry:3.11-alpine", "/bin/bash", "-c",
            "poetry install && python -c \"import nms_common\""
        ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        try:
            self.assertEqual(0, process.returncode)
        except:  # noqa: E722
            logging.error(stdout)
            logging.error(stderr)
            raise

    def test_install_locally_on_python_3_7(self):
        root = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..'))
        process = subprocess.Popen([
            "docker", "run", "-v", f"{root}:/library", "python:3.7", "/bin/bash", "-c",
            "pip install /library && python -c \"import nms_common\""
        ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        try:
            self.assertEqual(0, process.returncode)
        except:  # noqa: E722
            logging.error(stdout)
            logging.error(stderr)
            raise

    def test_install_gitlab_commit(self):
        process = subprocess.Popen([
            "docker", "run", "python:3.11", "/bin/bash", "-c",
            "pip install git+https://code.europa.eu/digit-c4/dev/python-nms-common.git && python -c \"import nms_common\""
        ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        try:
            self.assertEqual(0, process.returncode)
        except:  # noqa: E722
            logging.error(stdout)
            logging.error(stderr)
            raise


if __name__ == '__main__':
    unittest.main()
