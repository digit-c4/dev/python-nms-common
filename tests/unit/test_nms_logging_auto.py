import logging
import os
import unittest
from unittest.mock import patch
from io import StringIO


def reset_env():
    for e in ['NMS_APP_NAME', 'NMS_APP_TYPE', 'NMS_OWNER_NAME', 'NMS_OWNER_TYPE']:
        if e in os.environ:
            del os.environ[e]


class TestNmsLoggingAuto(unittest.TestCase):

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_ends_with_newline(self, stderr):
        reset_env()
        from nms_common import nms_logging_auto
        pid = os.getpid()

        logging.warning("blabetiblou")
        self.assertRegex(stderr.getvalue(),
                         "^\[[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}\]\[" + str(
                             pid) + "\]"
                                    "\[root\]"
                                    "\[WARNI\]"
                                    "\[team:ntx\]"
                                    "\[app:undefined\]\t")


if __name__ == '__main__':
    unittest.main()
