import logging as python_logging
import os
import unittest
from unittest.mock import patch
from io import StringIO

from nms_common import sloggly


def reset_env():
    for e in ['NMS_APP_NAME', 'NMS_APP_TYPE', 'NMS_OWNER_NAME', 'NMS_OWNER_TYPE']:
        if e in os.environ:
            del os.environ[e]


class TestFormatter(unittest.TestCase):

    def testFailWithoutArg(self):
        reset_env()
        with self.assertRaises(Exception):
            sloggly.Formatter()

    def testFailMissingAppNameArg(self):
        reset_env()
        os.environ['NMS_OWNER_NAME'] = 'api'
        with self.assertRaises(Exception) as e:
            sloggly.Formatter()

        self.assertEqual(str(e.exception), "Unknown app name, please set NMS_APP_NAME environment variable")

    def testFailMissingOwnerNameArg(self):
        reset_env()
        os.environ['NMS_APP_NAME'] = 'blabetiblou'
        with self.assertRaises(Exception) as e:
            sloggly.Formatter()

        self.assertEqual(str(e.exception), "Unknown owner name, please set NMS_OWNER_NAME environment variable")

    def testSucceedWithEnvironment(self):
        reset_env()
        os.environ['NMS_OWNER_NAME'] = 'me'
        os.environ['NMS_APP_NAME'] = 'blabetiblou'
        sloggly.Formatter()

    def testSucceedWithParam(self):
        reset_env()
        sloggly.Formatter(owner_name="me", app_name="blabetiblou")

    @patch('sys.stderr', new_callable=StringIO)
    def testCompatWithoutArgWorksWithWarnings(self, stderr):
        reset_env()
        python_logging.getLogger().handlers = [python_logging.StreamHandler()]

        sloggly.Formatter(compat=True)

        self.assertEqual(stderr.getvalue(), "Unknown app name, please set NMS_APP_NAME environment variable\n"
                                            "Unknown app type, please set NMS_APP_TYPE environment variable\n"
                                            "Unknown owner type, please set NMS_OWNER_TYPE environment variable\n"
                                            "Unknown owner name, please set NMS_OWNER_NAME environment variable\n")

    @patch('sys.stderr', new_callable=StringIO)
    def testCompatWithOneArgWorksWithWarnings(self, stderr):
        reset_env()
        os.environ['NMS_APP_NAME'] = '_'
        python_logging.getLogger().handlers = [python_logging.StreamHandler()]

        sloggly.Formatter(compat=True)
        self.assertEqual(stderr.getvalue(), "Unknown app type, please set NMS_APP_TYPE environment variable\n"
                                            "Unknown owner type, please set NMS_OWNER_TYPE environment variable\n"
                                            "Unknown owner name, please set NMS_OWNER_NAME environment variable\n")


class TestSloggly(unittest.TestCase):

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_ends_with_newline(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("_")
        self.assertTrue(stderr.getvalue().endswith("\n"))

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_ends_with_newline_empty_msg(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("")
        self.assertTrue(stderr.getvalue().endswith("\n"))

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_starts_with_iso_timestamp_header(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("_")
        self.assertRegex(stderr.getvalue(),
                         '^\[[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}\]\[[0-9]+\]')

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_pid_2nd_header(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("_")
        pid = os.getpid()
        self.assertRegex(stderr.getvalue(), f"^\[[^\[\]]*\]\[{pid}\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_logger_3rd_header(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_", name="blabetiblou").warning("_")
        self.assertRegex(stderr.getvalue(), "^(\[[^\[\]]*\]){2}\[blabetiblou\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_level_4th_header(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_", level=python_logging.DEBUG).debug("_")
        self.assertRegex(stderr.getvalue(), "^(\[[^\[\]]*\]){3}\[DEBUG\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_level_4th_header_padding(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_", level=python_logging.DEBUG).info("_")
        self.assertRegex(stderr.getvalue(), "^(\[[^\[\]]*\]){3}\[INFO \]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_level_4th_header_trim(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_", level=python_logging.DEBUG).warning("_")
        self.assertRegex(stderr.getvalue(), "^(\[[^\[\]]*\]){3}\[WARNI\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_app_6th_header(self, stderr):
        sloggly.getAndConfigureLogger(app_name="blabetiblou", owner_name="_", app_type="gui").warning("_")
        self.assertRegex(stderr.getvalue(), "^(\[[^\[\]]*\]){5}\[gui:blabetiblou\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_message(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("blabetiblou")
        self.assertEqual(stderr.getvalue().split("\t")[1], "blabetiblou\n")

    @patch('sys.stderr', new_callable=StringIO)
    def test_message_special_chars(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("blabetiblou\ncoucou\ntab\n")
        self.assertEqual(stderr.getvalue().split("\t")[1], "blabetiblou\\ncoucou\\ntab\\n\n")

    @patch('sys.stderr', new_callable=StringIO)
    def test_message_with_tab(self, stderr):
        sloggly.getAndConfigureLogger(app_name="_", owner_name="_").warning("blabetiblou\tcoucou\ttab\t")
        self.assertEqual(len(stderr.getvalue().split("\t")), 2)


if __name__ == '__main__':
    unittest.main()
