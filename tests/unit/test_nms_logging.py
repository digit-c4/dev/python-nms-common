import logging as python_logging
import os
import unittest
from unittest.mock import patch
from io import StringIO

from nms_common import nms_logging


class TestSloggly(unittest.TestCase):

    def setUp(self) -> None:
        os.environ['NMS_OWNER_NAME'] = 'ntx'

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_ends_with_newline(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning("_")
        self.assertTrue(stderr.getvalue().endswith("\n"))

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_ends_with_newline_empty_msg(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning("")
        self.assertTrue(stderr.getvalue().endswith("\n"))

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_starts_with_iso_timestamp_header(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning("_")
        self.assertRegex(stderr.getvalue(),
                         '^\[[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}\]\[[0-9]+\]')

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_line_pid_2nd_header(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning("_")
        pid = os.getpid()
        self.assertRegex(stderr.getvalue(),
                         f"^\[[^\[\]]*\]\[{pid}\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_logger_3rd_header(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, name="blabetiblou", app_name="_",
                                          owner_name="_").warning("_")
        self.assertRegex(stderr.getvalue(),
                         "^(\[[^\[\]]*\]){2}\[blabetiblou\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_level_4th_header(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.DEBUG, app_name="_", owner_name="_").debug("_")
        self.assertRegex(stderr.getvalue(),
                         "^(\[[^\[\]]*\]){3}\[DEBUG\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_level_4th_header_padding(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").info("_")
        self.assertRegex(stderr.getvalue(),
                         "^(\[[^\[\]]*\]){3}\[INFO \]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_log_level_4th_header_trim(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.DEBUG, app_name="_", owner_name="_").warning("_")
        self.assertRegex(stderr.getvalue(),
                         "^(\[[^\[\]]*\]){3}\[WARNI\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_app_6th_header(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_type="gui", app_name="blabetiblou",
                                          owner_name="_").warning("_")
        self.assertRegex(stderr.getvalue(),
                         "^(\[[^\[\]]*\]){5}\[gui:blabetiblou\]")

    @patch('sys.stderr', new_callable=StringIO)
    def test_message(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning(
            "blabetiblou")
        self.assertEqual(stderr.getvalue().split("\t")[1], "blabetiblou\n")

    @patch('sys.stderr', new_callable=StringIO)
    def test_message_special_chars(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning(
            "blabetiblou\ncoucou\ntab\n")
        self.assertEqual(stderr.getvalue().split("\t")[1], "blabetiblou\\ncoucou\\ntab\\n\n")

    @patch('sys.stderr', new_callable=StringIO)
    def test_message_with_tab(self, stderr):
        nms_logging.getAndConfigureLogger(level=python_logging.INFO, app_name="_", owner_name="_").warning(
            "blabetiblou\tcoucou\ttab\t")
        self.assertEqual(len(stderr.getvalue().split("\t")), 2)


if __name__ == '__main__':
    unittest.main()
