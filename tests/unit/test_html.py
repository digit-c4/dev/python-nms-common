import unittest
from nms_common import html


class TestHtml(unittest.TestCase):

    def test_two_lines(self):
        two_lines = "this is \n two lines"
        self.assertTrue(html.html_rendering(two_lines) == "this&nbsp;is&nbsp;<br/>&nbsp;two&nbsp;lines")

    def test_three_lines(self):
        three_lines = "this is \n three \n lines"
        self.assertTrue(html.html_rendering(three_lines) == "this&nbsp;is&nbsp;<br/>&nbsp;three&nbsp;<br/>&nbsp;lines")

    def test_block_lines(self):
        block_lines = """
            This is
            block lines
        """
        self.assertTrue(html.html_rendering(
            block_lines) == "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This&nbsp;is<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;block&nbsp;lines<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")


if __name__ == '__main__':
    unittest.main()
