import unittest
from requests.exceptions import SSLError, Timeout
from unittest.mock import Mock, patch

from nms_common import diego


class TestDiego(unittest.TestCase):

    # Info : Tested specifications.
    diego_inst = diego.Diego(user="user", password="password")

    def setUp(self) -> None:
        pass

    # Case 1 : DiegoRequest exceptions such as "SSLError, GenericError, TimeoutError" in "GET" and "POST"
    @patch('requests.get')
    def test_diego_request_return_ssl_error_get(self, requests_mock_get):
        requests_mock_get.side_effect = SSLError("My ssl error")
        with self.assertRaises(SSLError):
            self.diego_inst.diegoRequest(url="toto", method="GET", playload=dict())
            requests_mock_get.assert_called_once()

    @patch('requests.post')
    def test_diego_request_return_ssl_error_post(self, requests_mock_post):    
        requests_mock_post.side_effect = SSLError("My ssl error")
        with self.assertRaises(SSLError):
            self.diego_inst.diegoRequest(url="toto", method="POST", playload=dict())
            requests_mock_post.assert_called_once()

    @patch('requests.get')
    def test_diego_request_return_timeout_error_get(self, requests_mock_get):
        requests_mock_get.side_effect = Timeout("My timeout error")
        with self.assertRaises(Timeout):
            self.diego_inst.diegoRequest(url="toto", method="GET", playload=dict())
            requests_mock_get.assert_called_once()

    @patch('requests.post')
    def test_diego_request_return_timeout_error_post(self, requests_mock_post):    
        requests_mock_post.side_effect = Timeout("My timeout error")
        with self.assertRaises(Timeout):
            self.diego_inst.diegoRequest(url="toto", method="POST", playload=dict())
            requests_mock_post.assert_called_once()

    @patch('requests.get')
    def test_diego_request_return_generic_error_get(self, requests_mock_get):
        requests_mock_get.side_effect = Exception("My generic error")
        with self.assertRaises(Exception):
            self.diego_inst.diegoRequest(url="toto", method="GET", playload=dict())
            requests_mock_get.assert_called_once()

    @patch('requests.post')
    def test_diego_request_return_generic_error_post(self, requests_mock_post):    
        requests_mock_post.side_effect = Exception("My generic error")
        with self.assertRaises(Exception):
            self.diego_inst.diegoRequest(url="toto", method="POST", playload=dict())
            requests_mock_post.assert_called_once()



if __name__ == '__main__':
    unittest.main()