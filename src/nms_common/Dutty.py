#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# encoding: utf8

import sys
import os
import configparser
import socket
import json
import logging
import requests
from pprint import pformat
from . import sloggly

hostname = os.uname()[1]
fqdn = socket.getfqdn()
whoami = sys._getframe().f_code.co_name
script = os.path.basename(__file__).split(".")[0]


logger = sloggly.setup_custom_logger(script, logging.DEBUG)


class Dutty(object):
    def __init__(self, *args, **kwargs):

        # remove proxy if present to avoid.
        if 'http_proxy' in os.environ:
            del os.environ['http_proxy']
        if 'https_proxy' in os.environ:
            del os.environ['https_proxy']

        # force a non local backend...
        if 'fqdn' in kwargs:
            self.fqdn = kwargs['fqdn']
        else:
            fqdn = socket.getfqdn()
            if '.dev.' in fqdn:
                self.fqdn = 'vworker0-lu.acc.snmc.cec.eu.int'
            else:
                self.fqdn = fqdn

        self.verbose = False
        if 'verbose' in kwargs:
            self.verbose = kwargs['verbose']

        # for now.
        self.verbose = True

        if self.verbose is True:
            logger.setLevel(logging.DEBUG)

        self.url = 'https://' + self.fqdn + '/snet/api/DUTTY/v1/message'
        self.method = 'PUT'

        requests_log = logging.getLogger("requests.packages.urllib3")
        ''' Set just the URLLIB3 logging level to INFO '''
        requests_log.setLevel(logging.INFO)
        requests_log.propagate = True
        # requests.packages.urllib3.disable_warnings()
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("urllib3").setLevel(logging.WARNING)

        self.VERIFY = '/etc/ssl/certs/ca-certificates.crt'
        if 'verify' in kwargs and kwargs['verify'] is True:
            self.VERIFY = '/etc/ssl/certs/ca-certificates.crt'
        elif 'verify' in kwargs and kwargs['verify'] is False:
            self.VERIFY = False

    def get_obj_message(self):

        # team = 'digit-snet-net', digit-snet-sec, digit-snet-sup
        obj_message = dict()
        obj_message['name'] = 'empty_caller'  # The title
        obj_message['id'] = 1
        obj_message['type'] = None  # the category
        # the function whoami = sys._getframe().f_code.co_name
        obj_message['supplier'] = None
        obj_message['error'] = None  # The error message
        obj_message['team'] = 'unknown'  # The team who should investigate
        # The snet process taking care of the event
        obj_message['actor'] = 'empty_actor'
        obj_message['author'] = hostname  # The script name

        return obj_message

    def put_error(self, obj_message):

        if obj_message is None:
            return False

        headers = {
            'Content-Type': 'application/json'
        }

        try:
            response = requests.put(self.url,
                                    json=obj_message,
                                    allow_redirects=True,
                                    headers=headers,
                                    verify=self.VERIFY)

        except requests.exceptions.SSLError as e:
            logger.error('%s: %s' % (self.url, str(e)))
            return False
        except Exception as e:
            logger.error('Generic: %s: %s' % (str(self.url), str(e)))
            return False

        if response.status_code == 200:
            return True
        else:
            logger.debug("Dutty NOT 200: %s" % (response.status_code))
            if self.verbose:
                logger.error('ERROR: not a 200.')
                try:
                    logger.error(response.status_code)
                except:
                    pass
                try:
                    logger.error(response.content)
                except:
                    pass
                try:
                    logger.error('URL %s %s' % (self.method, self.url))
                except:
                    pass
                try:
                    logger.error('Playload:%s' % (json.dumps(
                        obj_message, sort_keys=True, indent=4)))
                except:
                    pass
                try:
                    logger.error('HEADERS:' % (pformat(response.headers)))
                except:
                    pass
                try:
                    logger.error('TEXT:' % (pformat(response.text)))
                except:
                    pass
            return False
        return False
