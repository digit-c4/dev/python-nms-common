#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def html_rendering(log):
    """
    returns html content

    Args:
        log :


    """
    loghtml = log
    loghtml = "<br/>".join(loghtml.split("\n"))
    loghtml = loghtml.replace(' ', '&nbsp;')
    return loghtml
