# -*- coding: utf-8 -*-
# Info : Native import
# import inspect
import logging

# Info : External lib import
import sqlalchemy

# Info : Local lib import
from . import sloggly


def setup_custom_logger(name):
    """
        DEPRECATED FUNCTION (16/11/2023)
    """
    logging.warning(
        "nms_common.common:setup_custom_logger is deprecated, call sloggy.setup_custom_logger() instead!")
    return sloggly.setup_custom_logger(name, "DEBUG")


# Without this, MySQL will silently insert invalid values in the
# database, causing very long debugging sessions in the long run
class DontBeSilly(sqlalchemy.interfaces.PoolListener):

    def connect(self, dbapi_con, connection_record):
        cur = dbapi_con.cursor()
        cur.execute("SET SESSION sql_mode='TRADITIONAL'")
        cur = None
