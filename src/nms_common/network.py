# -*- coding: utf-8 -*-
import socket
import struct


class Network:

    def makeMask(self, n):
        "return a mask of n bits as a long integer"
        return (2L << n - 1) - 1

    def dottedQuadToNum(self, ip):
        "convert decimal dotted quad string to long integer"
        return struct.unpack('L', socket.inet_aton(ip).zfill(8))[0]

    def networkMask(self, ip, bits):
        "Convert a network address to a long integer"
        return self.dottedQuadToNum(ip) & self.makeMask(bits)

    def networkMask_cidr(self, cidr):
        "Convert a network address to a long integer"
        # print "CIDR:" + cidr
        ip, bits = cidr.split('/')
        # print "net:" + ip +  " bits:" + bits
        return self.dottedQuadToNum(ip) & self.makeMask(int(bits))

    def addressInNetwork(self, ip, net):
        "Is an address in a network"
        return ip & net == net

    def addressInNetwork_str(self, ip, net):
        "Is an address in a network"
        ipaddr = struct.unpack('L', socket.inet_aton(ip))[0]
        netaddr, bits = net.split('/')
        netmask = struct.unpack('L', socket.inet_aton(netaddr))[
            0] & ((2L << int(bits) - 1) - 1)
        return ipaddr & netmask == netmask
