# -*- coding: utf-8 -*-
from sqlalchemy.exc import *
from sqlalchemy import *
import re
# from cache import scached
import datetime
import os
import sys


path = os.path.dirname(sys.argv[0])


class SID_DeviceScheduledIntegration:

    def __init__(self, db_url):
        self.db_url = db_url
        self.engine = create_engine(self.db_url)
        self.connection = self.engine.connect()

    def __del__(self):
        try:
            self.connection.close()
        except Exception as e:
            print('Error %s' % (str(e)))
            pass
        finally:
            print('Connection close, alles gut!')

    # @scached(cache_file=path + '/proteusmanagedobject.db', expiry=datetime.timedelta(hours=2))
    def getObject(self, devices):
        query = f"SELECT FILE_UID, FILE_NAME, OBJECT_UID, OBJECT_NAME, CREATIONDATE, AGE, SCHEDULED, REALAGE, ERROR \
  FROM {os.getenv('SID_TEST_TABLE')} \
  WHERE \
      OBJECT_NAME in (" + str(devices) + ") "

        result = []
        try:
            result = self.connection.execute(query).fetchall()
        except DBAPIError as e:
            # an exception is raised, Connection is invalidated.
            if e.connection_invalidated:
                print("Connection was invalidated!")

            # after the invalidate event, a new connection
            # starts with a new Pool
            self.connection = self.engine.connect()
            result = self.connection.execute(query).fetchall()
            pass
        except Exception as e:
            print('Error %s' % (str(e)))
            pass
        finally:
            print('')

        return result


class SID_DeviceByVendor:

    def __init__(self, db_url):
        self.db_url = db_url
        self.engine = create_engine(self.db_url)
        self.rawconnection = self.engine.raw_connection()

    def __del__(self):
        try:
            self.rawconnection.close()
        except Exception as e:
            print('Error %s' % (str(e)))
            pass
        finally:
            print('Connection close, alles gut!')

    # @scached(cache_file=path + '/proteusmanagedobject.db', expiry=datetime.timedelta(hours=2))
    def getObject(self, vendoruid):
        result = []
        try:
            in_cursor = self.rawconnection.connection.cursor()
            out_cursor = self.rawconnection.connection.cursor()
            in_cursor.execute(
                'begin dbi.GETDEVICEBYVENDOR(' + vendoruid + ',:CSR); end;', CSR=out_cursor)
            result = out_cursor.fetchall()
        except Exception as e:
            print('Error %s' % (str(e)))
            pass
        finally:
            print('')

        return result
