# -*- coding: utf-8 -*-

# import configparser as ConfigParser
import pprint
import os
import json
from .ldapwrapper import LDAPWrapper
# from .config import Config


class AccessDenied(Exception):
    pass


class Access(object):

    def __init__(self, headers):
        '''headers as returned by request.headers
            thus the input is a dictionary of HTML header variables

           Example header (nginx -> uwsgi):
           {'HTTP_X_FORWARDED_POPULATION': ''
            'HTTP_REFERER': 'https://<dev env>/'
            'REDIRECT_STATUS': ''
            'SERVER_SOFTWARE': ''
            'SCRIPT_NAME': ''
            'REQUEST_METHOD': 'GET'
            'PATH_INFO': ''
            'SERVER_PROTOCOL': 'HTTP/1.1'
            'HTTP_PRAGMA': 'no-cache'
            'CONTENT_LENGTH': ''
            'HTTP_USER_AGENT': ''
            'HTTP_CONNECTION': ''
            'SERVER_NAME': '<dev server name>'
            'REMOTE_PORT': ''
            'HTTP_CLIENT_IP': ''
            'HTTP_X_BLUECOAT_VIA': ''
            'SERVER_PORT': ''
            'SERVER_ADDR': ''
            'DOCUMENT_ROOT': ''
            'HTTP_AUTHUSER': 'uid=alogin,ou=,o='
            'HTTP_AUTHGROUP': 'cudgroup==<agroup>'
            'HTTP_X_FORWARDED_PROTO': ''
            'SCRIPT_FILENAME': ''
            'HTTP_DNT': '1'
            'HTTP_HOST': '<dev serv name>'
            'HTTPS': 'on'
            'HTTP_CACHE_CONTROL': 'no-cache'
            'REQUEST_URI': ''
            'HTTP_ACCEPT': ''
            'GATEWAY_INTERFACE': ''
            'HTTP_X_FORWARDED_FOR': ''
            'REMOTE_ADDR': ''
            'HTTP_ACCEPT_LANGUAGE': ''
            'HTTP_ACCEPT_ENCODING': ''
            }
        '''

        self.aclauthgroup = False
        self.pp = pprint.PrettyPrinter(indent=4)
        self.e = None

        self.authGroup = []
        self.authUser = ''
        try:
            self.authUser = headers.get('Authuser', '')
        except:
            ''' Failed to fetch the header '''
            pass

        try:
            self.authGroup = headers.get('Authgroup', '')
        except:
            ''' Failed to fetch the header '''
            pass

        try:
            if self.authUser is None or len(self.authUser) == 0:
                self.authUser = headers.get('HTTP_AUTHUSER', '')
        except:
            ''' Failed to fetch the header '''
            pass

        try:
            if self.authGroup is None or len(self.authGroup) == 0:
                self.authGroup = headers.get('HTTP_AUTHGROUP', '')
        except:
            ''' Failed to fetch the header '''
            pass

        if self.authUser is None:
            self.authUser = ''
        if self.authGroup is None:
            self.authGroup = []

        try:
            self._cleanUserGroup()
        except Exception as e:
            '''If there is no user/group info in the headers or if something else went
            wrong, assume request without auth'''
            self.e = str(e)

        # WARNING : HERE Lay sensitive content.
        self.snetsup = list()

    def __str__(self):
        return '<Access AuthUser=%s, AuthGroup=%s, e=%s>' % (self.authUser, self.authGroup, self.e)

    def _cleanUserGroup(self):
        '''Here we strip known useless text from the variables'''

        if self.authGroup.startswith('cudgroup=='):
            self.authGroup = self.authGroup.replace('cudgroup==', '')

        elif ',' in self.authGroup:
            '''If the group as returned from the proxy is a comma seperated list, convert it
            to an array'''
            self.authGroup = self.authGroup.replace(' ', '').split(',')

        if self.authUser and 'o=cec.eu.int' in self.authUser:
            self.authUser = self.authUser.replace('uid=', '')
            self.authUser = self.authUser.replace(' ', '')\
                                         .replace(',ou=People,o=cec.eu.int', '')\
                                         .replace(',ou=people,o=cec.eu.int', '')

            try:
                ldap = LDAPWrapper()
                self.authGroup = ldap.getUserGroups(self.authUser)
                ldap.disconnect()
            except Exception as e:
                ''' Nothing '''
                self.e = str(e)
                print(e)

    def checkAuthorization(self, script):
        '''
            This is called to check for authorization as defined in the H. vault.
        '''
        access_config = os.getenv('ACCESS_SECRET')
        if not access_config:
            raise Exception("ACCESS Secrets missing from server environment")
        if isinstance(access_config, str):
            acl = json.loads(os.getenv('ACCESS_SECRET'))
        else:
            acl = os.getenv('ACCESS_SECRET')

        '''check if any of the groups which are permitted to access
        the script match the groups the user is in'''
        for g in acl:
            if g in self.authGroup:
                self.aclauthgroup = g
                # if so, return True
                return True

        # else raise an exception
        raise AccessDenied('Access Denied: %s' % (str(self)))

    def get_group_authorised(self):
        return '%s' % (self.aclauthgroup)

    def get_groups(self):
        return '%s' % (self.authGroup)

    def get_user(self):
        return '%s' % (self.authUser)

    """ Reason : NOT usable in the package version.
    def is_snet_sup(self):
        if self.authUser in self.snetsup:
            return True
        return False
    """
