# -*- coding: utf-8 -*-
import os
import sys
import logging
import requests
from requests.auth import HTTPBasicAuth
import socket


class Scrat:

    # def __init__(self, user, password, version=1, verify=True, verbose=False):
    def __init__(self, user, password, *args, **kwargs):
        # remove proxy if present to avoid.
        if 'http_proxy' in os.environ:
            del os.environ['http_proxy']
        if 'https_proxy' in os.environ:
            del os.environ['https_proxy']

        # force a backend...
        if 'fqdn' in kwargs:
            fqdn = kwargs['fqdn']
        else:
            fqdn = socket.getfqdn()

        self.SCRAT_USER = user
        self.SCRAT_PASSWD = password

        # Info (From PETJERE) : Support v2.2.9 role-based access.
        self.SCRAT_GROUP = None
        if 'group' in kwargs:
            self.SCRAT_GROUP = kwargs['group']

        self.verbose = False
        if 'verbose' in kwargs:
            self.verbose = kwargs['verbose']

        # logger
        self.logger = logging.getLogger(__name__)
        formatter = logging.Formatter(fmt="%(asctime)s - [%(levelname)-5.5s] - %(name)s %(module)s:%(funcName)s"
                                      " - L%(lineno)d - "
                                      "%(message)s", datefmt='%H:%M:%S')
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(formatter)
        self.logger.addHandler(stdout_handler)
        self.logger.setLevel(logging.DEBUG)

        requests_log = logging.getLogger("requests.packages.urllib3")
        ''' Set just the URLLIB3 logging level to INFO '''
        requests_log.setLevel(logging.INFO)
        requests_log.propagate = True
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("urllib3").setLevel(logging.WARNING)

        # Info : Update for scrat v2.
        if 'version' in kwargs:
            self.SCRAT_VERSION = kwargs['version']
        else:
            self.SCRAT_VERSION = 1

        self.SCRAT_URL = 'https://{f}/snet/scrat/api/snetis/v{vs}/object'.format(
            f=fqdn, vs=self.SCRAT_VERSION)

        self.VERIFY = os.getenv("GLOBAL_CHAIN_CERT_LOC")
        if 'verify' in kwargs and kwargs['verify'] is True:
            self.VERIFY = os.getenv("GLOBAL_CHAIN_CERT_LOC")
        elif 'verify' in kwargs and kwargs['verify'] is False:
            self.VERIFY = False

    # Info (From PETJERE) : Adding two generic methods I use everywhere.
    def _set_scrat_user(self, new_user):
        self.SCRAT_USER = new_user

    def _db_change(self, author, obj, query_mode="creation",
                   overwrite_mode=False, line_only=False):
        """ Generic method for scrat queries """
        if author != self.SCRAT_USER:
            self._set_scrat_user(author)

        if query_mode == "delete":
            if line_only:
                res_code = self.deleteLine(obj)
            else:
                res_code = self.deleteName(obj["context"], obj["name"], False)
        else:
            res_code = self.scratQuery(obj, query_mode=query_mode,
                                       overwrite_mode=overwrite_mode,
                                       line_only=line_only)
        return res_code

    def deleteUID(self, context, uid, force=False, cascade=False):
        result = True

        if self.SCRAT_VERSION == 2:
            url = self.SCRAT_URL + "/uid/delete"
            force = False
            # cascade = False
        else:
            url = self.SCRAT_URL + "/" + str(context) + "/" + str(uid) + "?"

        if force:
            url += "force=true&"
        if cascade:
            url += "oncascade=true"

        try:
            headers = {
                'Content-Type': 'application/json'
            }
            if self.SCRAT_GROUP:
                headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
            if self.SCRAT_VERSION == 2:
                params = {"context": context, "uid": uid}
                response = requests.delete(url,
                                           auth=HTTPBasicAuth(
                                               self.SCRAT_USER, self.SCRAT_PASSWD),
                                           allow_redirects=True,
                                           headers=headers,
                                           json=params,
                                           verify=self.VERIFY)

                result = response.status_code

            else:
                response = requests.delete(url,
                                           auth=HTTPBasicAuth(
                                               self.SCRAT_USER, self.SCRAT_PASSWD),
                                           allow_redirects=True,
                                           headers=headers,
                                           verify=self.VERIFY)
        except Exception as e:
            return {'status': 'Error', 'message': 'Sorry: Generic Exception: %s' % (e)}, 500

        return response.json(), result

    def deleteLine(self, json_attrib):

        if self.SCRAT_VERSION != 2:
            return {'status': 'Error', 'message': "This function is not compatible with your scrat version"}

        # Info : Delete by line always use option 'line_only'.
        json_attrib['line_only'] = 'True'
        url = self.SCRAT_URL + "/name/delete"

        headers = {
            'Content-Type': 'application/json'
        }
        if self.SCRAT_GROUP:
            headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
        response = requests.delete(url,
                                   auth=HTTPBasicAuth(
                                       self.SCRAT_USER, self.SCRAT_PASSWD),
                                   allow_redirects=True,
                                   headers=headers,
                                   json=json_attrib,
                                   verify=self.VERIFY)
        return response.json(), response.status_code

    def deleteName(self, context, name, cascade):

        if self.SCRAT_VERSION != 2:
            return {'status': 'Error', 'message': "This function is not compatible with your scrat version"}

        params = {"context": context, "name": name}

        # Info : Delete by name never use option 'line_only'.
        url = self.SCRAT_URL + "/name/delete"
        if cascade:
            url += "oncascade=true"

        headers = {
            'Content-Type': 'application/json'
        }
        if self.SCRAT_GROUP:
            headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
        response = requests.delete(url,
                                   auth=HTTPBasicAuth(
                                       self.SCRAT_USER, self.SCRAT_PASSWD),
                                   allow_redirects=True,
                                   headers=headers,
                                   json=params,
                                   verify=self.VERIFY)
        return response.json(), response.status_code

    def scratQuery(self, params, query_mode="creation", overwrite_mode=False, line_only=False):
        if query_mode == "creation":
            try:
                headers = {
                    'Content-Type': 'application/json'
                }
                if self.SCRAT_GROUP:
                    headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
                response = requests.post(self.SCRAT_URL,
                                         auth=HTTPBasicAuth(
                                             self.SCRAT_USER, self.SCRAT_PASSWD),
                                         allow_redirects=True,
                                         headers=headers,
                                         json=params,
                                         verify=self.VERIFY)

                if self.SCRAT_VERSION == 2:
                    if response.status_code != 200:
                        if self.verbose:
                            self.logger.error(response.text)
                        return response.text, response.status_code

                    return response.json(), response.status_code

            except requests.exceptions.SSLError as e:
                self.logger.error('SSL Error occurs: {}'.format(e.message))
                return {'status': 'Error', 'message': "Sorry: SSL exception: %s" % (e)}, 500
            except Exception as e:
                self.logger.error('Generic: %s' % (str(e)))
                return {'status': 'Error', 'message': 'Sorry: Generic Exception: %s' % (e)}, 500
        elif query_mode == "update" and self.SCRAT_VERSION == 2:
            response = self.scratUpdate(params, overwrite_mode, line_only)
            return response
        else:
            return {'status': 'Error', 'message': "Unrecognized version of scrat detected !"}

        return response.text

    def scratUpdate(self, params, overwrite_mode=False, line_only=False):

        header = {'content-type': 'application/json'}
        if self.SCRAT_GROUP:
            header["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP

        scratUpdateUrl = self.SCRAT_URL + "/" + str(overwrite_mode)
        scratUpdateUrl += '/' + str(line_only)
        req = requests.put(scratUpdateUrl,
                           json=params,
                           headers=header,
                           auth=HTTPBasicAuth(
                               self.SCRAT_USER, self.SCRAT_PASSWD),
                           allow_redirects=True,
                           verify=self.VERIFY)

        if req.status_code != 200:
            if self.verbose:
                self.logger.error(req.text)
            return req.text, req.status_code

        return req.json(), req.status_code
