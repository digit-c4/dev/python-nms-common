# -*- coding: utf-8 -*-

import os
from ldap3 import Server, Connection

# from nms_common.config import Config


class LDAPWrapper(object):

    '''LDAP wrapper class abstracting some of the raw LDAP code
    for simple operations'''

    '''Initialize this class with or without parameters.
  If none are specified, set default values found in the LDAP
  ini file.
  Upon initialization, an LDAP connection is established.

  '''

    def __init__(self, server=None, user=None, password=None):
        if server is None and user is None and password is None:
            server_name = os.getenv("LDAP_SERVER_NAME")
            server_port = os.getenv("LDAP_SERVER_PORT")
            user = os.getenv("LDAP_SERVER_USER")
            password = os.getenv("LDAP_SERVER_PASSWORD")
            self.basedn = os.getenv("LDAP_CONFIG_BASE")

        self.server = Server(server_name, port=int(server_port), use_ssl=False)
        self.con = Connection(self.server, user, password, auto_bind=True)

    def __del__(self):
        if self.con:
            try:
                self.con.unbind()
            except:
                pass

    def disconnect(self):
        if self.con:
            try:
                self.con.unbind()
            except:
                pass

    '''This method returns the groups as a list, a user is member of
  as stored in LDAP.
  If no basedn is specified, use the one set in the LDAP ini file.
  If no attribute is specified, fall bak to cudgroup (CUD).
  If no groups are found in LDAP, return an empty list.

  '''

    def getUserGroups(self, user, basedn=None, attribute=None):
        if basedn is None:
            basedn = self.basedn

        if attribute is None:
            attributes = ['cudgroup']
        else:
            attributes = [attribute]

        filter = '(uid=' + str(user) + ')'

        self.con.search(search_base=basedn,
                        search_filter=filter,
                        attributes=attributes)

        groups = []
        for entry in self.con.response:
            if attributes[0] in entry['attributes']:
                for g in entry['attributes'][attributes[0]]:
                    groups.append(g)
        print(f"Whole response: {self.con.response}")
        print(f"Groups found : {groups}")

        return groups

    '''This method checks whether a user is member of a specific group
  while simply returning True or False'''

    def hasGroup(self, user, group, basedn=None, attribute=None):
        groups = self.getUserGroups(user, basedn, attribute)

        if group in groups:
            return True

        return False
