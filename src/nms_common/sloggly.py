# -*- coding: utf-8 -*-
""" Python SNET generic client for logs. """
import logging
import logging.handlers
import os
import sys

_DEFAULT_APP_TYPE = "app"
_DEFAULT_APP_NAME = "undefined"
_DEFAULT_OWNER_TYPE = "team"
_DEFAULT_OWNER_NAME = "ntx"


def snet_formater():
    """
    Create a new Snet Logging Formatter.

    Returns:
        an logging formater


    """
    logging.warning(
        """slogging.snet_formater is deprecated, please use nms_common.sloggly.Formatter(app_type, app_name) instead."""
    )
    return Formatter()


class Formatter(logging.Formatter):
    def __init__(self, app_type=None, app_name=None, owner_type=None, owner_name=None, compat=False):
        """
            Format with last version of the SNET log standard format (14/11/2023)
            With uwsgi rsyslog forwarding implementation, it will produce the format :
            <Mon> <DD> <HH>:<MM>:<SS> <HOST> DEV_SQUAD_<APPNAME>[<PID>]: [<LOG.TYPE> ] - <PythonModule>:<PythonMethod>
            - L<Line> - <LogMessage>

            Args:
                (optional) name : Name of your logger.

            Returns:
                a logging formater

            Raises:
                Void
        """

        app_name = os.getenv("NMS_APP_NAME") if app_name is None else app_name
        app_type = os.getenv("NMS_APP_TYPE") if app_type is None else app_type
        owner_type = os.getenv(
            "NMS_OWNER_TYPE") if owner_type is None else owner_type
        owner_name = os.getenv(
            "NMS_OWNER_NAME") if owner_name is None else owner_name

        if app_name is None or app_name == "":
            msg = "Unknown app name, please set NMS_APP_NAME environment variable"
            if compat:
                logging.warning(msg)
                app_name = _DEFAULT_APP_NAME
            else:
                raise Exception(msg)

        if app_type is None or app_type == "":
            logging.warning(
                "Unknown app type, please set NMS_APP_TYPE environment variable")
            app_type = _DEFAULT_APP_TYPE

        if owner_type is None or owner_type == "":
            logging.warning(
                "Unknown owner type, please set NMS_OWNER_TYPE environment variable")
            owner_type = _DEFAULT_OWNER_TYPE

        if owner_name is None or owner_name == "":
            msg = "Unknown owner name, please set NMS_OWNER_NAME environment variable"
            if compat:
                logging.warning(msg)
                owner_name = _DEFAULT_OWNER_NAME
            else:
                raise Exception(msg)

        super().__init__(
            fmt=f"[%(asctime)s.%(msecs)03d][%(process)d][%(name)s][%(levelname)-5.5s]"
                f"[{owner_type}:{owner_name}][{app_type}:{app_name}]\t%(message)s",
            datefmt="%Y-%m-%dT%H:%M:%S")

    def formatMessage(self, record):
        record.message = record.message.encode(
            "unicode_escape").decode("utf-8")
        return super().formatMessage(record)


def setup_custom_logger(name, log_level=None):
    logging.warning(
        "nms_common.sloggy.setup_custom_logger() is depreacated. Use nms_common.sloggy.getLogger() instead.")

    return getLogger(None, None, name, log_level)


class NmsStreamHandler(logging.StreamHandler):
    default_handler = None

    def __init__(self, owner_type=None, owner_name=None, app_type=None, app_name=None, compat=False):
        super().__init__()

        self.configure_formatter(owner_type=owner_type, owner_name=owner_name, app_type=app_type, app_name=app_name,
                                 compat=compat)

    def configure_formatter(self, owner_type=None, owner_name=None, app_type=None, app_name=None, compat=False):
        self.setFormatter(Formatter(owner_type=owner_type, owner_name=owner_name, app_type=app_type,
                                    app_name=app_name, compat=compat))


def configure_handler(owner_type=None, owner_name=None, app_type=None, app_name=None, compat=False):
    if NmsStreamHandler.default_handler is None:
        NmsStreamHandler.default_handler = NmsStreamHandler(owner_type=owner_type, owner_name=owner_name,
                                                            app_type=app_type,
                                                            app_name=app_name, compat=compat)
    else:
        NmsStreamHandler.default_handler.configure_formatter(owner_type=owner_type, owner_name=owner_name,
                                                             app_type=app_type,
                                                             app_name=app_name, compat=compat)

    return NmsStreamHandler.default_handler


def default_handler(owner_type=None, owner_name=None, app_type=None, app_name=None, compat=False):
    logging.warning(
        "default_handler() is deprecated because confusing. Use configure_handler() instead.")
    return configure_handler(owner_type=owner_type, owner_name=owner_name, app_type=app_type, app_name=app_name,
                             compat=compat)


def _get_logger(name=None, level=None):
    level = os.getenv("LOG_LEVEL", logging.INFO) if level is None else level
    name = os.getenv("LOG_LOGGER", "root") if name is None else name

    logger = logging.getLogger(name)
    logger.setLevel(level)

    return logger


def getLogger(name=None, level=None):
    logger = _get_logger(name=name, level=level)

    handler = NmsStreamHandler.default_handler
    if handler is None:
        handler = NmsStreamHandler(compat=True)

    # This line is needed for test mocking of sys.stderr to work.
    # Otherwise, NmsStreamHandler will not use the mocked version
    handler.setStream(sys.stderr)
    logger.handlers = [handler]

    return logger


def getAndConfigureLogger(name=None, level=None, owner_type=None, owner_name=None, app_type=None, app_name=None):
    """
    Setup a logger.

    Args:
        app_type: the type of the application. Example "api", "gui", "script", "job", etc.
        app_name: the name of the application
        name : Name of the application using the logger.
        level : Requested minimal severity of log, must be one of the level given by logging.getLevelNamesMapping()

    Returns:
        an initialized logger

    """
    logger = _get_logger(name=name, level=level)

    handler = configure_handler(
        owner_type=owner_type, owner_name=owner_name, app_type=app_type, app_name=app_name)
    # This line is needed for test mocking of sys.stderr to work.
    # Otherwise, NmsStreamHandler will not use the mocked version
    handler.setStream(sys.stderr)
    logger.handlers = [handler]

    return logger


def basicConfig(owner_type=None, owner_name=None, app_type=None, app_name=None, level=None, compat=False):
    handler = configure_handler(
        owner_type=owner_type, owner_name=owner_name, app_type=app_type, app_name=app_name, compat=compat)
    # This is needed for unit testing mocking of sys.stderr to work
    handler.setStream(sys.stderr)

    logging.basicConfig(
        level=level if level is not None else os.getenv(
            "LOG_LEVEL", logging.INFO),
        handlers=[handler],
        force=True
    )
