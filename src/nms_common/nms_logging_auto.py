""" Syntax sugar for Python SNET generic client to format logs. """
from . import nms_logging

nms_logging.basicConfig(compat=True)
