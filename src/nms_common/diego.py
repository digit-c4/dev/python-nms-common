# -*- coding: utf-8 -*-
import os
import sys
import json
import re
import logging
import configparser
from pprint import pformat
import requests
from requests.auth import HTTPBasicAuth
# from flask import abort
import socket


class Diego:

    def __init__(self, *args, **kwargs):

        # Info : remove proxy if present to avoid.
        if 'http_proxy' in os.environ:
            del os.environ['http_proxy']
        if 'https_proxy' in os.environ:
            del os.environ['https_proxy']
        # Update (nms_common) : remove as well UPPERCASE values as it is considered as disctinct values !
        if 'HTTP_PROXY' in os.environ:
            del os.environ['HTTP_PROXY']
        if 'HTTPS_PROXY' in os.environ:
            del os.environ['HTTPS_PROXY']

        # force a backend...
        if 'fqdn' in kwargs:
            fqdn = kwargs['fqdn']
        else:
            fqdn = socket.getfqdn()
        whoami = sys._getframe().f_code.co_name

        self.DIEGO_USER = ""
        if 'user' in kwargs:
            self.DIEGO_USER = kwargs['user']
        else:
            raise Exception("Missing mandatory user input in diego call")

        # Info (From PETJERE) : Support v2.2.9 role-based access.
        self.DIEGO_GROUP = None
        if 'group' in kwargs:
            self.DIEGO_GROUP = kwargs['group']

        self.DIEGO_PASSWD = ""
        if 'password' in kwargs:
            self.DIEGO_PASSWD = kwargs['password']
        else:
            raise Exception("Missing mandatory password input in diego call")

        self.DIEGO_URL = ('https://%s/snet/diego/api/snetis/v1' % (fqdn))
        self.DIEGO_URL_v2 = ('https://%s/snet/diego/api/snetis/v2' % (fqdn))

        self.verbose = False
        if 'verbose' in kwargs:
            self.verbose = kwargs['verbose']

        # logger
        self.logger = logging.getLogger(__name__)
        formatter = logging.Formatter(fmt="%(asctime)s - [%(levelname)-5.5s] - %(name)s %(module)s:%(funcName)s"
                                      " - L%(lineno)d - "
                                      "%(message)s", datefmt='%H:%M:%S')
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(formatter)
        self.logger.addHandler(stdout_handler)
        self.logger.setLevel(logging.DEBUG)

        requests_log = logging.getLogger("requests.packages.urllib3")
        ''' Set just the URLLIB3 logging level to INFO '''
        requests_log.setLevel(logging.INFO)
        requests_log.propagate = True
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("urllib3").setLevel(logging.WARNING)

        self.VERIFY = os.getenv("GLOBAL_CHAIN_CERT_LOC")
        if 'verify' in kwargs and kwargs['verify']:
            self.VERIFY = kwargs['verify']
        else:
            self.VERIFY = False

    # Info (From PETJERE) : Adding one generic method I use everywhere.
    def dieget(self, d_name, params):
        """ Shortcut for diego mono-method """
        return self.diego_run_dieget_by_name(d_name, params)

    def cleanParams(self, param):
        if param is not None:
            param.replace('"', '')
            param.replace('%', '')
        else:
            param = ''
        return param

    def diegoRequest(self, method, url, playload, version=None):
        """
            Updated for role-based access.
        """
        # Diego query
        # b64, I know...
        if version is not None and str(version) == '2':
            DiegoUrlQuery = ('%s/%s' % (self.DIEGO_URL_v2, url))
        else:
            DiegoUrlQuery = ('%s/%s' % (self.DIEGO_URL, url))
        headers = {
            'Content-Type': 'application/json'
        }
        if self.DIEGO_GROUP:
            headers["Authgroup"] = "cudgroup==" + self.DIEGO_GROUP
        # self.logger.debug('URL %s %s' % (method, DiegoUrlQuery))
        # self.logger.debug('Playload:%s' % (pformat(playload)))

        if method == 'GET':
            try:
                response = requests.get(DiegoUrlQuery,
                                        auth=HTTPBasicAuth(
                                            self.DIEGO_USER, self.DIEGO_PASSWD),
                                        allow_redirects=True,
                                        headers=headers,
                                        verify=self.VERIFY)
            except requests.exceptions.SSLError as e:
                self.logger.error('%s: %s' % (DiegoUrlQuery, str(e)))
                raise
            except Exception as e:
                self.logger.error('Generic: %s' % (str(e)))
                raise

        elif method == 'POST':
            if playload is not None:
                data = playload
            else:
                data = {}
            try:
                # self.logger.error("DIEGO query done" + pformat(data))
                response = requests.post(DiegoUrlQuery,
                                         auth=HTTPBasicAuth(
                                             self.DIEGO_USER, self.DIEGO_PASSWD),
                                         json=data,
                                         allow_redirects=True,
                                         headers=headers,
                                         verify=self.VERIFY)
                # if self.verbose:
                #    self.logger.error("DIEGO query done: " + pformat(response.content))
            except requests.exceptions.SSLError as e:
                self.logger.error('%s: %s: %s' %
                                  (DiegoUrlQuery, type(e), str(e)))
                raise
            except Exception as e:
                self.logger.error('Generic: %s: %s: %s' %
                                  (str(DiegoUrlQuery), type(e), str(e)))
                self.logger.error('Playload:%s' % (pformat(data)))
                raise

        else:
            self.logger.error('Unknown methods.')
            return None

        if response.status_code == 200:
            data = response.content
            # self.logger.debug("DIEGO 200")
        else:
            # self.logger.debug("DIEGO NOT 200: %s" % (response.status_code))
            # return abort(404)
            if self.verbose:
                self.logger.error('ERROR: not a 200.')
                self.logger.error(response.status_code)
                self.logger.error(response.content)
                self.logger.error('URL %s %s' % (method, DiegoUrlQuery))
                self.logger.error('Playload:%s' % (pformat(data)))
                self.logger.error('HISTORY: %s' % (pformat(response.history)))
                self.logger.error('HEADERS: %s' % (pformat(response.headers)))
                self.logger.error('TEXT: %s' % (pformat(response.text)))
            # return abort(404)
            return None

        # self.logger.error('data:%s' % pformat(data))
        try:
            data_str = data.decode('utf-8')
            # self.logger.debug("DECODE\n")
        except Exception as e:
            self.logger.error("Error bytes to str converstion: %s" % (str(e)))
            pass

        '''"headers" "results"'''
        try:
            contexts = json.loads(data_str)
        except Exception as e:
            self.logger.error(
                "Error during the json data loading: %s" % (str(e)))
            raise
        if self.verbose:
            self.logger.info(contexts)

        if 'error' in contexts and contexts['error'] is not None:
            self.logger.error("Error during the data fetching")
            raise Exception("Error during the data fetching")

        # self.logger.debug(pformat(contexts));
        return contexts

    def diegoAclChecker(self, type, context):

        data = dict()
        data['type'] = type
        data['context'] = context
        # self.logger.error(pformat(data))
        try:
            results = self.diegoRequest('POST', 'acl', data, version=1)
        except Exception as e:
            self.logger.error('diegoAclChecker: Exception %s (%s)' %
                              (str(e), pformat(data)))
            results = dict()
            results['type'] = type
            results['context'] = context
            results['RO'] = False
            results['RW'] = False
            results['error'] = True
            return (results)
        if results:
            return (results)
        else:
            self.logger.error('diegoAclChecker: not ok :%s' % (results))
            if not results or not isinstance(results, dict):
                results = dict()
            results['type'] = type
            results['context'] = context
            results['RO'] = False
            results['RW'] = False
            results['error'] = True
            return (results)

    def diegoQuery(self, query, Full=False):
        whoami = sys._getframe().f_code.co_name
        data = {'query': query}
        try:
            results = self.diegoRequest('POST', 'query', data)
        except Exception as e:
            self.logger.error('%s: Exception %s (%s)' %
                              (whoami, str(e), pformat(data)))
            return (None, None)
        if results:
            if Full is False:
                return (results['headers'], results['results'])
            else:
                return (results['headers'], results)
        else:
            self.logger.error('%s: not ok %s' % (whoami, str(query)))
            return (None, None)

    def JSONtreediegoQuery(self, query, Full=False):
        whoami = sys._getframe().f_code.co_name
        data = {'query': query}
        try:
            results = self.diegoRequest('POST', 'query', data)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            # abort(e)
            return (None)
        if results:
            # self.logger.debug("SENDING BACK RES" + pformat(results))
            if Full is False:
                return (results['results'])
            else:
                return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def FromSidCreateTemplateQuery(self, query):
        re_compiled_search_in = re.compile(r'VALUE\s::\s:[a-zA-Z]')
        replace_in = False
        if re_compiled_search_in.search(query):
            replace_in = True

        re_compiled_search = re.compile(r'(:[a-zA-Z][^:]+:)')

        # Info : Old way of parsing dieget.
        for m in re_compiled_search.finditer(query):

            # pat = pat.replace(' ', '_')
            pat = str(m.group(0))
            pat = pat.replace(':', '"{', 1)
            pat = pat.replace(':', '}"', 1)

            query = query.replace(str(m.group(0)), pat, 1)

        # need to cover the case with ' :: ' in the query
        if replace_in:
            re_compiled_search = re.compile(r'(:: "{[a-zA-Z][^{]+}")')
            for m in re_compiled_search.finditer(query):
                pat = str(m.group(0))
                pat = pat.replace('"{', '{').replace('}"', '}')
                query = query.replace(str(m.group(0)), pat, 1)

        return query

    # Info : New way of parsing dieget
    def FromSidParseTemplateQuery(self, query, params=None):

        re_compiled_search = re.compile(r'(:[a-zA-Z][^:]+:)')

        for m in re_compiled_search.finditer(query):
            n = m.group(0)
            var_name = n[1:-1]
            assert var_name in params, "Error: Missing variable '{v}' in parameters".format(
                v=var_name)
            var_value = params[var_name]

            if self.is_numeric(var_value):
                query = re.sub(':' + var_name + ':', str(var_value), query)
            else:
                if isinstance(var_value, list):
                    full_ctt = (
                        ','.join('"' + item + '"' for item in var_value))
                    query = re.sub(':' + var_name + ':',
                                   '(' + full_ctt + ')', query)
                else:
                    query = re.sub(':' + var_name + ':', '"' +
                                   var_value + '"', query)

        return query

    def is_numeric(self, value):

        res = None
        if isinstance(value, int):
            res = True
        elif isinstance(value, str):
            res = re.search(r'^\d*$', value)
        return res is not None

    def diego_run_dieget_by_name(self, dieget_name, param, Full=False):

        dieget_request = '''SELECT TYPE "database query" (
        full "has as value" as "dieget"
) as "dieget_name" HIDETYPE HIDEUID FILTER ( (CONTEXT = 666000002) AND  (VALUE = :dieget_name:) )
TRANSFORM JSONFLAT'''

        # replace placeholder to python one.
        dieget_request = self.FromSidCreateTemplateQuery(dieget_request)
        dieget_request = dieget_request.format(dieget_name=dieget_name)

        try:
            (header, results) = self.diegoQuery(dieget_request)
        except requests.exceptions.SSLError as e:
            self.logger.error('%s: %s: %s' %
                              (DiegoUrlQuery, type(e), str(e)))
            raise
        except Exception as e:
            self.logger.error('Generic: %s: %s: %s' %
                              (str(DiegoUrlQuery), type(e), str(e)))
            self.logger.error('Playload:%s' % (pformat(data)))
            raise

        if not results or len(results[0]) < 2:
            raise Exception("Dieget '{}' not found".format(
                            dieget_name), dieget_request)
        dieget_real = results[0][1]
        # dieget_request = self.FromSidCreateTemplateQuery(dieget_real, param)
        dieget_request = self.FromSidParseTemplateQuery(dieget_real, param)

        # self.logger.info("dieget inspection : {d}".format(d=dieget_request))

        return self.diegoQuery(dieget_request, Full)

    def diegoGetObject(self, uid, context_uid=None, ExtractContext=False):
        whoami = sys._getframe().f_code.co_name
        if not self.is_numeric(uid):
            self.logger.error(
                '%s: uid expected to be a numeric (%s)' % (whoami, str(uid)))
            return (None)
        if context_uid is not None and not self.is_numeric(context_uid):
            self.logger.error('%s: context_uid expected to be a numeric (%s)' % (
                whoami, str(context_uid)))
            return (None)

        call_url = '/object/' + str(uid)
        if context_uid is not None:
            call_url += '/' + str(context_uid)
        try:
            results = self.diegoRequest('GET', call_url, None, 2)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoGetNetworkRole(self, object_name):
        whoami = sys._getframe().f_code.co_name
        call_url = '/network_role/' + str(object_name)
        try:
            results = self.diegoRequest('GET', call_url, None, 1)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoGetIpSubnet(self, ip, context_uid=None):
        whoami = sys._getframe().f_code.co_name
        if context_uid is not None and not self.is_numeric(context_uid):
            self.logger.error('%s: context_uid expected to be a numeric (%s)' % (
                whoami, str(context_uid)))
            return (None)

        call_url = '/ip_to_subnet/' + str(ip)
        call_url += '/' + str(context_uid)
        try:
            results = self.diegoRequest('GET', call_url, None, 1)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoGetIpBlock(self, ip, context_uid=None):
        whoami = sys._getframe().f_code.co_name
        if context_uid is not None and not self.is_numeric(context_uid):
            self.logger.error('%s: context_uid expected to be a numeric (%s)' % (
                whoami, str(context_uid)))
            return (None)

        call_url = '/ip_to_block/' + str(ip)
        call_url += '/' + str(context_uid)
        try:
            results = self.diegoRequest('GET', call_url, None, 1)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoGetIpDmzDeploy(self, ip, context_uid=None):
        whoami = sys._getframe().f_code.co_name
        if context_uid is not None and not self.is_numeric(context_uid):
            self.logger.error('%s: context_uid expected to be a numeric (%s)' % (
                whoami, str(context_uid)))
            return (None)

        call_url = '/ip_to_dmz_deploy/' + str(ip)
        call_url += '/' + str(context_uid)
        try:
            results = self.diegoRequest('GET', call_url, None, 1)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoGetIpDmz(self, ip, context_uid=None):
        whoami = sys._getframe().f_code.co_name
        if context_uid is not None and not self.is_numeric(context_uid):
            self.logger.error('%s: context_uid expected to be a numeric (%s)' % (
                whoami, str(context_uid)))
            return (None)

        call_url = '/ip_to_dmz/' + str(ip)
        call_url += '/' + str(context_uid)
        try:
            results = self.diegoRequest('GET', call_url, None, 1)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoGetIpRoute(self, ip, context_uid=None):
        whoami = sys._getframe().f_code.co_name
        if context_uid is not None and not self.is_numeric(context_uid):
            self.logger.error('%s: context_uid expected to be a numeric (%s)' % (
                whoami, str(context_uid)))
            return (None)

        call_url = '/ip_to_route/' + str(ip)
        call_url += '/' + str(context_uid)
        try:
            results = self.diegoRequest('GET', call_url, None, 1)
        except Exception as e:
            self.logger.error('%s: Exception %s' % (whoami, str(e)))
            return (None)
        if results:
            return (results)
        else:
            self.logger.error('%s: not ok' % (whoami))
            return (None)

    def diegoObjectStructure(self, object_name):

        data = dict()
        data['reference'] = object_name
        # self.logger.error(pformat(data))
        try:
            results = self.diegoRequest(
                'POST', '/object/structure', data, version=1)
        except Exception as e:
            self.logger.error(
                'diegoObjectStructure: Exception %s (%s)' % (str(e), pformat(data)))
            results = dict()
            results['typename'] = object_name
            results['error'] = True
            return (results)

        if results is None:
            self.logger.error(
                'Empty stucture object. The structure could not be determined.')
            results = dict()
            results['typename'] = object_name
            results['error'] = True
            return (results)

        if results:
            return (results)

        else:
            self.logger.error('diegoObjectStructure: not ok :%s' % (results))
            if not results or not isinstance(results, dict):
                results = dict()

            results['typename'] = object_name
            results['error'] = True
            return (results)
