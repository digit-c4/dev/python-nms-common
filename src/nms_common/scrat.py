# -*- coding: utf-8 -*-
import os
import sys
import logging
import requests
from requests.auth import HTTPBasicAuth
import socket
from pprint import pformat


class Scrat:

    # def __init__(self, user, password, version=1, verify=True, verbose=False):
    def __init__(self, user, password, *args, **kwargs):
        """
            Initialize requester.
            In this module, version 1 and 2 of Scrat are the version supported.
            See with the management if disagreement.
        """
        if 'http_proxy' in os.environ:
            del os.environ['http_proxy']
        if 'https_proxy' in os.environ:
            del os.environ['https_proxy']

        # force a backend...
        if 'fqdn' in kwargs:
            fqdn = kwargs['fqdn']
        else:
            fqdn = socket.getfqdn()

        self.SCRAT_USER = user
        self.SCRAT_PASSWD = password

        # Info (From PETJERE) : Support v2.2.9 role-based access.
        self.SCRAT_GROUP = None
        if 'group' in kwargs:
            self.SCRAT_GROUP = kwargs['group']

        self.verbose = False
        if 'verbose' in kwargs:
            self.verbose = kwargs['verbose']

        # logger
        self.logger = logging.getLogger(__name__)
        formatter = logging.Formatter(fmt="%(asctime)s - [%(levelname)-5.5s] - %(name)s %(module)s:%(funcName)s"
                                      " - L%(lineno)d - "
                                      "%(message)s", datefmt='%H:%M:%S')
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(formatter)
        self.logger.addHandler(stdout_handler)
        self.logger.setLevel(logging.DEBUG)

        requests_log = logging.getLogger("requests.packages.urllib3")
        ''' Set just the URLLIB3 logging level to INFO '''
        requests_log.setLevel(logging.INFO)
        requests_log.propagate = True
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("urllib3").setLevel(logging.WARNING)

        # Info : Update for scrat v2.
        if 'version' in kwargs:
            self.SCRAT_VERSION = kwargs['version']
        else:
            self.SCRAT_VERSION = 1

        self.SCRAT_URL = 'https://{f}/snet/scrat/api/snetis/v{vs}/object'.format(
            f=fqdn, vs=self.SCRAT_VERSION)

        self.VERIFY = os.getenv("GLOBAL_CHAIN_CERT_LOC")
        if 'verify' in kwargs and kwargs['verify'] is True:
            self.VERIFY = os.getenv("GLOBAL_CHAIN_CERT_LOC")
        elif 'verify' in kwargs and kwargs['verify'] is False:
            self.VERIFY = False

    def deleteUID(self, context, uid, force=False, cascade=False, Full=False):
        """
            Updated for role-based access.
        """
        result = True

        if self.SCRAT_VERSION == 2:
            url = self.SCRAT_URL + "/uid/delete"
            # Info : TEMP
            force = False
            cascade = False
        else:
            url = self.SCRAT_URL + "/" + str(context) + "/" + str(uid) + "?"

        if force:
            url += "force=true&"
        if cascade:
            url += "oncascade=true"

        try:
            headers = {
                'Content-Type': 'application/json'
            }
            if self.SCRAT_GROUP:
                headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
            if self.SCRAT_VERSION == 2:
                params = {"context": context, "uid": uid}
                response = requests.delete(url,
                                           auth=HTTPBasicAuth(
                                               self.SCRAT_USER, self.SCRAT_PASSWD),
                                           allow_redirects=True,
                                           headers=headers,
                                           json=params,
                                           verify=self.VERIFY)

                result = response.status_code

            else:
                response = requests.delete(url,
                                           auth=HTTPBasicAuth(
                                               self.SCRAT_USER, self.SCRAT_PASSWD),
                                           allow_redirects=True,
                                           headers=headers,
                                           verify=self.VERIFY)
        except Exception as e:
            if Full:
                return {'status': 'Error', 'message': 'Sorry: Generic Exception: %s' % (e)}, 500
            result = False
            raise
        if Full:
            return response.json(), result
        return result

    def deleteLine(self, json_attrib, Full=False):

        if self.SCRAT_VERSION != 2:
            return {'status': 'Error', 'message': "This function is not compatible with your scrat version"}

        # Info : Delete by line always use option 'line_only'.
        json_attrib['line_only'] = 'True'
        url = self.SCRAT_URL + "/name/delete"

        headers = {
            'Content-Type': 'application/json'
        }
        if self.SCRAT_GROUP:
            headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
        response = requests.delete(url,
                                   auth=HTTPBasicAuth(
                                       self.SCRAT_USER, self.SCRAT_PASSWD),
                                   allow_redirects=True,
                                   headers=headers,
                                   json=json_attrib,
                                   verify=self.VERIFY)
        if Full:
            return response.json(), response.status_code

        return response.status_code

    def deleteName(self, context, name, cascade, Full=False):

        if self.SCRAT_VERSION != 2:
            return {'status': 'Error', 'message': "This function is not compatible with your scrat version"}

        params = {"context": context, "name": name}

        # Info : Delete by name never use option 'line_only'.
        url = self.SCRAT_URL + "/name/delete"
        if cascade:
            url += "oncascade=true"

        headers = {
            'Content-Type': 'application/json'
        }
        if self.SCRAT_GROUP:
            headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
        response = requests.delete(url,
                                   auth=HTTPBasicAuth(
                                       self.SCRAT_USER, self.SCRAT_PASSWD),
                                   allow_redirects=True,
                                   headers=headers,
                                   json=params,
                                   verify=self.VERIFY)
        if Full:
            return response.json(), response.status_code

        return response.status_code

    def scratQuery(self, params, query_mode="creation", overwrite_mode=False, line_only=False, Full=False):
        if query_mode == "creation":
            try:
                headers = {
                    'Content-Type': 'application/json'
                }
                if self.SCRAT_GROUP:
                    headers["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP
                response = requests.post(self.SCRAT_URL,
                                         auth=HTTPBasicAuth(
                                             self.SCRAT_USER, self.SCRAT_PASSWD),
                                         allow_redirects=True,
                                         headers=headers,
                                         json=params,
                                         verify=self.VERIFY)

                if self.SCRAT_VERSION == 2:
                    if response.status_code != 200 and self.verbose:
                        self.logger.error(response.text)
                    if Full:
                        return response.json(), response.status_code

                    return response.status_code

            except requests.exceptions.SSLError as e:
                self.logger.error('SSL Error occurs: {}'.format(e.message))
                if Full:
                    return {'status': 'Error', 'message': "Sorry: SSL exception: %s" % (e)}, 500
                return None
            except Exception as e:
                self.logger.error('Generic: %s' % (str(e)))
                if Full:
                    return {'status': 'Error', 'message': 'Sorry: Generic Exception: %s' % (e)}, 500
                return None
        elif query_mode == "update" and self.SCRAT_VERSION == 2:
            response = self.scratUpdate(
                params, overwrite_mode, line_only, Full)
            return response
        else:
            if Full:
                return {'status': 'Error', 'message': 'Sorry: Unrecognized version of scrat detected'}, 500
            raise Exception("Unrecognized version of scrat detected !")

        # BUG! response not defined!
        return (response.text)

    def scratUpdate(self, params, overwrite_mode=False, line_only=False, Full=False):

        header = {'content-type': 'application/json'}
        if self.SCRAT_GROUP:
            header["Authgroup"] = "cudgroup==" + self.SCRAT_GROUP

        scratUpdateUrl = self.SCRAT_URL + "/" + str(overwrite_mode)
        scratUpdateUrl += '/' + str(line_only)
        req = requests.put(scratUpdateUrl,
                           json=params,
                           headers=header,
                           auth=(self.SCRAT_USER, self.SCRAT_PASSWD),
                           allow_redirects=True,
                           verify=self.VERIFY)

        if req.status_code != 200 and self.verbose:
            self.logger.error('ERROR: not a 200.')
            self.logger.error(req.status_code)
            self.logger.error(req.content)
            self.logger.error('URL %s %s' % ('PUT', scratUpdateUrl))
            self.logger.error('Playload:%s' % (pformat(params)))
            self.logger.error('HISTORY: %s' % (pformat(req.history)))
            self.logger.error('HEADERS: %s' % (pformat(req.headers)))
            self.logger.error('REQUEST HEADERS: %s' %
                              (pformat(req.request.headers)))
            self.logger.error('TEXT: %s' % (pformat(req.text)))

        if Full:
            return req.json(), req.status_code

        return req.status_code
